package graph

import (
	"fmt"
)

// Any type implementing the Graph interface can make use of the
// functions defined in this package.
type Graph interface {
	// Returns all Nodes in the Graph
	Nodes() []Node

	// Returns all Edges in the Graph
	Edges() []Edge

	// Returns the Edge between two Nodes. If no such edge is present,
	// the second argument is false.
	Edge(a, b Node) (Edge, bool)

	// Returns a slice of all Nodes adjacent to the given Node.
	Neighbors(n Node) []Node
}

type DirectedGraph Graph
type UndirectedGraph Graph

type GraphConstructor func(nodes []Node, edges []Edge) Graph

// Any type implementing the Node interface can be used to define a
// Graph. All implentations of Node should be useable as map keys.
type Node interface {
	fmt.Stringer
	Equals(other Node) bool
}

type Edge interface {
	Head() Node
	Tail() Node
}
