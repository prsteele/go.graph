package graph

// A Graph implementation that uses a map of maps to associate (Node,
// Node) pairs with the Edge that contains them.
type basicGraph struct {
	// A set of all Nodes in the Graph
	nodes map[Node]bool

	// A list of all Edges in the Graph
	edges map[Edge]bool

	// A map of (Node, Node) pairs to their associated Edge
	neighbors map[Node]map[Node]Edge

	// Is this graph undirected?
	undirected bool
}

func (g basicGraph) Edge(a, b Node) (Edge, bool) {
	if g.undirected {
		if e, ok := g.neighbors[a][b]; ok {
			return e, true
		}
		e, ok := g.neighbors[b][a]
		return e, ok
	}

	e, ok := g.neighbors[a][b]
	return e, ok
}

func (g basicGraph) Neighbors(n Node) []Node {
	neighbors := make([]Node, 0)
	if adjacent := g.neighbors[n]; adjacent != nil {
		for k, _ := range adjacent {
			neighbors = append(neighbors, k)
		}
	}
	return neighbors
}

func (g basicGraph) Nodes() []Node {
	nodes := make([]Node, 0)
	for k, v := range g.nodes {
		if v {
			nodes = append(nodes, k)
		}
	}

	return nodes
}

func (g basicGraph) Edges() []Edge {
	edges := make([]Edge, 0)
	for k, v := range g.edges {
		if v {
			edges = append(edges, k)
		}
	}

	return edges
}

func (g *basicGraph) insertEdge(e Edge) {
	head := e.Head()
	tail := e.Tail()

	if g.undirected {
		g.edges[e] = true
		if g.neighbors[tail] == nil {
			g.neighbors[tail] = make(map[Node]Edge)
		}
		if g.neighbors[head] == nil {
			g.neighbors[head] = make(map[Node]Edge)
		}
		g.neighbors[tail][head] = e
		g.neighbors[head][tail] = e

	} else {
		g.edges[e] = true
		if g.neighbors[tail] == nil {
			g.neighbors[tail] = make(map[Node]Edge)
		}
		g.neighbors[tail][head] = e
	}
}

func NewGraph(nodes []Node, edges []Edge) Graph {
	g := new(basicGraph)
	g.nodes = make(map[Node]bool)
	g.edges = make(map[Edge]bool)
	g.neighbors = make(map[Node]map[Node]Edge)

	// Add all nodes
	for _, n := range nodes {
		g.nodes[n] = true
	}

	// Add all edges, and associate nodes with edges in neighbors
	for _, edge := range edges {
		g.insertEdge(edge)
	}

	return g
}

type basicUndirectedGraph struct {
	basicGraph
}

func NewUndirectedGraph(nodes []Node, edges []Edge) UndirectedGraph {
	g := new(basicGraph)
	g.undirected = true
	g.nodes = make(map[Node]bool)
	g.edges = make(map[Edge]bool)
	g.neighbors = make(map[Node]map[Node]Edge)

	// Add all nodes
	for _, n := range nodes {
		g.nodes[n] = true
	}

	// Add all edges, and associate nodes with edges in neighbors
	for _, edge := range edges {
		g.insertEdge(edge)
	}

	return g
}

type E struct {
	H Node
	T Node
}

func (e E) Head() Node {
	return e.H
}

func (e E) Tail() Node {
	return e.T
}
