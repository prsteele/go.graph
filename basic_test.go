package graph

import (
	"testing"
)

func TestNewEdge(t *testing.T) {
	tail := NewIntNode(1)
	head := NewIntNode(2)

	edge := E{T: tail, H: head}

	if edge.Head().Equals(head) == false {
		t.Errorf("Expected .Head() to equal %v, not %v", head, edge.Head())
	}

	if edge.Tail().Equals(tail) == false {
		t.Errorf("Expected .Tail() to equal %v, not %v", tail, edge.Tail())
	}
}

func TestNewGraph(t *testing.T) {
	nodes := []Node{NewIntNode(1), NewIntNode(2), NewIntNode(3)}
	edges := []Edge{E{T: nodes[0], H: nodes[1]},
		E{T: nodes[1], H: nodes[2]}}

	g := NewGraph(nodes, edges)

	neighbors := g.Neighbors(nodes[0])
	if len(neighbors) != 1 || neighbors[0].Equals(nodes[1]) == false {
		t.Errorf("Expected neighbors to be %v, not %v", nodes[1:2], neighbors)
	}
}

func TestNewUndirectedGraph(t *testing.T) {
	const (
		N = 10
	)

	nodes := make([]Node, 0)
	for i := 0; i < N; i++ {
		nodes = append(nodes, *NewIntNode(i))
	}

	edges := make([]Edge, 0)
	for i, _ := range nodes[:len(nodes)-1] {
		edges = append(edges, E{nodes[i], nodes[i+1]})
	}

	g := NewUndirectedGraph(nodes, edges)

	for i := 1; i < N-1; i++ {
		if neighbors := g.Neighbors(nodes[i]); len(neighbors) != 2 {
			t.Errorf("Expected 2 neighbors, not %d (=%v)", len(neighbors), neighbors)
		}

		if _, ok := g.Edge(nodes[i], nodes[i-1]); !ok {
			t.Errorf("Expected edge (%s, %s) to be present", nodes[i], nodes[i-1])
		}

		if _, ok := g.Edge(nodes[i-1], nodes[i]); !ok {
			t.Errorf("Expected edge (%s, %s) to be present", nodes[i-1], nodes[i])
		}

		if _, ok := g.Edge(nodes[i], nodes[i+1]); !ok {
			t.Errorf("Expected edge (%s, %s) to be present", nodes[i], nodes[i+1])
		}

		if _, ok := g.Edge(nodes[i+1], nodes[i]); !ok {
			t.Errorf("Expected edge (%s, %s) to be present", nodes[i+1], nodes[i])
		}
	}
}
