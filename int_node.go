package graph

import (
	"fmt"
)

type IntNode struct {
	V int
}

func (n IntNode) Equals(other Node) bool {
	switch t := other.(type) {
	case IntNode:
		return n.V == t.V
	case *IntNode:
		return n.V == t.V
	}
	return false
}

func NewIntNode(i int) *IntNode {
	return &IntNode{V: i}
}

func (n IntNode) String() string {
	return fmt.Sprintf("%d", n.V)
}
