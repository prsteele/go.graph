go.graph
========

A simple library for graph datastructures.

Use
===

Install via `go get github.com/patrickrsteele/go.graph'. The package
name for use in code is `graph'.