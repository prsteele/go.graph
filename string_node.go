package graph

type StringNode struct {
	S string
}

func (n StringNode) Equals(other Node) bool {
	if s, ok := other.(StringNode); ok {
		return n.S == s.S
	}
	return false
}

func (n StringNode) String() string {
	return n.S
}
