package graph

import (
	"testing"
)

func TestNewIntNode(t *testing.T) {
	const (
		value = 7
	)

	inode := NewIntNode(value)

	n := (interface{})(inode)
	if _, ok := n.(Node); !ok {
		t.Error("IntNode does not implement the Node interface")
	}

	if inode.V != value {
		t.Errorf("Expected value %i, received %v", value, inode.V)
	}
}
