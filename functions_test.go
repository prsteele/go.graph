package graph

import (
	"testing"
)

// We test breadth-first search on the following directed graph. All
// edges (u, v) are oriented such that u < v.
//
//    1
//   /|\
//  0 3 4
//   \|/
//    2
//
func TestBreadthFirstSearch_1(t *testing.T) {
	nodes := []Node{NewIntNode(0), NewIntNode(1), NewIntNode(2),
		NewIntNode(3), NewIntNode(4)}
	edges := []Edge{E{T: nodes[0], H: nodes[1]},
		E{T: nodes[0], H: nodes[2]}, E{T: nodes[1], H: nodes[3]},
		E{T: nodes[2], H: nodes[3]}, E{T: nodes[1], H: nodes[4]},
		E{T: nodes[2], H: nodes[4]}}

	g := NewGraph(nodes, edges)

	bfs := BreadthFirstSearch(g, nodes[0])

	if len(bfs) != len(nodes) {
		t.Fatalf("Expected breadth-first search to find %d nodes, not %d",
			len(nodes), len(bfs))
	}

	if bfs[0].Equals(nodes[0]) == false {
		t.Errorf("Expected first node to be %v, not %v", nodes[0], bfs[0])
	}

	if ((bfs[1].Equals(nodes[1]) && bfs[2].Equals(nodes[2])) ||
		(bfs[1].Equals(nodes[2]) && bfs[2].Equals(nodes[1]))) == false {
		t.Errorf("Expected second and third nodes to be %v and %v, not %v and %v",
			nodes[1], nodes[2], bfs[1], bfs[2])
	}

	if ((bfs[3].Equals(nodes[3]) && bfs[4].Equals(nodes[4])) ||
		(bfs[3].Equals(nodes[4]) && bfs[4].Equals(nodes[3]))) == false {
		t.Errorf("Expected second and third nodes to be %v and %v, not %v and %v",
			nodes[3], nodes[4], bfs[3], bfs[4])
	}
}

// We test breadth-first search on the following directed graph. All
// edges (u, v) are oriented such that u < v.
//
// 0 - 1 - ... - (n - 1)
//
func TestBreadthFirstSearch_2(t *testing.T) {
	const (
		n = 100
	)

	// Build the graph
	nodes := make([]Node, n)
	edges := make([]Edge, n-1)
	for i, _ := range nodes {
		nodes[i] = NewIntNode(i)

		if i > 0 {
			edges[i-1] = E{T: nodes[i-1], H: nodes[i]}
		}
	}
	g := NewGraph(nodes, edges)

	for i, n := range nodes {
		bfs := BreadthFirstSearch(g, n)

		for j, m := range bfs {
			if m.Equals(nodes[i+j]) == false {
				t.Errorf("Expected %v, received %v", nodes[i+1], m)
			}
		}
	}
}

// We test ConnectedComponents on a zero-edge graph
func TestConnectedComponents_1(t *testing.T) {
	const (
		N = 10
	)

	nodes := make([]Node, 0)
	for i := 0; i < N; i++ {
		nodes = append(nodes, *NewIntNode(i))
	}

	g := NewUndirectedGraph(nodes, []Edge{})
	components := ConnectedComponents(g)

	if len(components) != N {
		t.Errorf("Expected there to be %d components, not %d", N, len(components))
	}

	for _, component := range components {
		if len(component) != 1 {
			t.Errorf("Expected component to have 1 element, not %d", len(component))
		}
	}

	node_set := make(map[Node]bool)
	for _, node := range nodes {
		node_set[node] = false
	}

	for _, component := range components {
		for _, node := range component {
			if _, ok := node_set[node]; !ok {
				t.Errorf("Unknown Node in component: %v", node)
			}

			node_set[node] = true
		}
	}

	for k, v := range node_set {
		if !v {
			t.Errorf("Node %s not found in connected components", k)
		}
	}
}

// We test ConnectedComponents on the union of two line graphs
func TestConnectedComponents_2(t *testing.T) {
	const (
		N = 10
	)

	nodes := make([]Node, 0)
	for i := 0; i < N; i++ {
		nodes = append(nodes, *NewIntNode(i))
	}

	edges := make([]Edge, 0)

	for i := 0; i < N/2; i++ {
		edges = append(edges, E{T: nodes[i], H: nodes[i+1]})
	}

	for i := N/2 + 1; i < N-1; i++ {
		edges = append(edges, E{T: nodes[i], H: nodes[i+1]})
	}

	g := NewUndirectedGraph(nodes, edges)
	components := ConnectedComponents(g)

	if len(components) != 2 {
		t.Errorf("Expected there to be 2 components, not %d", len(components))
	}

	node_set := make(map[Node]bool)
	for _, node := range nodes {
		node_set[node] = false
	}

	for _, component := range components {
		for _, node := range component {
			if _, ok := node_set[node]; !ok {
				t.Errorf("Unknown Node in component: %v", node)
			}

			node_set[node] = true
		}
	}

	for k, v := range node_set {
		if !v {
			t.Errorf("Node %s not found in connected components", k)
		}
	}
}
