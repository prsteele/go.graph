package graph

// Lists all Nodes found in a breadth-first search of the graph,
// beginning at node n.
func BreadthFirstSearch(g Graph, source Node) []Node {
	type Color int
	const (
		unvisited = iota
		visited
	)

	// Note that color[n] == unvisited for all Nodes n, since
	// unvisited=0 is the default value for Color
	color := make(map[Node]Color)

	// The queue of nodes to be explored
	queue := make([]Node, 0)
	queue = append(queue, source)
	color[source] = visited

	// All discovered nodes, in breadth-first order
	nodes := make([]Node, 0)

	// Explore until there are no nodes left in the queue
	for len(queue) > 0 {
		current := queue[0]
		queue = queue[1:]
		nodes = append(nodes, current)

		// Explore all neighbors
		for _, n := range g.Neighbors(current) {
			if color[n] == unvisited {
				color[n] = visited
				queue = append(queue, n)
			}
		}
	}

	return nodes
}

// Computes the connected components of a Graph. Returns each
// component as a slice of Nodes. Note that this 
func ConnectedComponents(g UndirectedGraph) [][]Node {
	nodes := make(map[Node]bool)
	for _, node := range g.Nodes() {
		nodes[node] = true
	}

	components := make([][]Node, 0)

	for len(nodes) > 0 {
		// Get the next node to start a search at
		var node Node
		for n, _ := range nodes {
			node = n
			break
		}

		// Find the next connected component
		component := BreadthFirstSearch(g, node)

		// Delete all nodes that have been found
		for _, n := range component {
			delete(nodes, n)
		}

		// Add the new component
		components = append(components, component)
	}

	return components
}
